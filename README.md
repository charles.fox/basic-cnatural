# Simple CNatural

An initial implementation of CNatural in Flex and Bison.

CNatural is an alternative to C++ that aims to remove any code redundancy from
the language.

This brief implementation provides Python-like code blocks, using indentation
rather than curly brackets.

Compile with `make`, and run with `./cn [sourcefile...]`.
